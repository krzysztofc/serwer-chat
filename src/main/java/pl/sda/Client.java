package pl.sda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static final int PORT = 9002;
    public static String hostname = "localhost";

    public static void main(String[] args) {

        try {
            Socket s = new Socket(hostname, PORT);

            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

            String inLine = in.readLine();
            System.out.println("RECEIVED: " + inLine);

            String name = "Krzysztof Ciebiada";
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                name = scanner.nextLine();
                break;
            }

            out.println(name);

            // Podaj imie i nazwisko

            inLine = in.readLine();
            System.out.println("RECEIVED: " + inLine);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
